# uuid-v4-regex

Tiny module providing a regex for use in validating UUID V4s.

## Usage

```bash
$ yarn add uuid-v4-regex
```

```javascript
const { regex } = require('uuid-v4-regex')

regex.test('f0827d4c-308a-4327-8018-b52116710fbe')
```