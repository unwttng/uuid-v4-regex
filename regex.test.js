const { regex } = require('./regex')

describe('regex', () => {
  test('validates a valid lowercase UUID', () => {
    expect('a54db5cb-1b74-4194-b3b5-0092f9712077').toMatch(regex)
    expect('c312a1f3-8d84-470f-97a1-1c5bff925e9a').toMatch(regex)
    expect('0a403620-e736-4355-a1b6-3baf7ef5109f').toMatch(regex)
    expect('4f648f75-9889-4aba-b185-4d5cfdb9affb').toMatch(regex)
    expect('b85f8343-a899-4af6-8d33-fd1dacdf3a2e').toMatch(regex)
  })

  test('validates a valid uppercase UUID', () => {
    expect('A54DB5CB-1B74-4194-B3B5-0092F9712077').toMatch(regex)
  })
})